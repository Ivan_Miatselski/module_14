﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pizzeria.PizzaFactory;
using Pizzeria.Kinds_of_pizzas;

namespace Pizzeria
{
    public class Pizzeria
    {
        private static Pizzeria pizzeriaInstance;
        private static object syncRoot = new Object();
        private List<IClient> clients = new List<IClient>();

        public AbstractFactory Factory { get; protected set; }

        protected Pizzeria(AbstractFactory factory)
        {
            if (factory == null)
            {
                throw new ArgumentNullException("Factory cannot be null");
            }

            this.Factory = factory;
        }

        public static Pizzeria GetInstance(AbstractFactory factory)
        {
            if (pizzeriaInstance == null)
            {
                lock (syncRoot)
                {
                    if (pizzeriaInstance == null)
                    {
                        pizzeriaInstance = new Pizzeria(factory);
                    }
                }
            }

            return pizzeriaInstance;
        }

        public void SubscribeToSendingNotification(IClient client)
        {
            if (client == null)
            {
                throw new ArgumentNullException("SubscribeToSendingNotification : Object client cannot be null");
            }

            clients.Add(client);
        }

        public void Accept(IVisitor visitor, Pizza pizza)
        {
            if (visitor == null || pizza == null)
            {
                throw new ArgumentNullException("Accept : parameters cannot be null");
            }

            if (clients.Count != 0)
            {
                foreach (IClient client in clients)
                    client.Accept(visitor, pizza);
            }
        }
    }
}

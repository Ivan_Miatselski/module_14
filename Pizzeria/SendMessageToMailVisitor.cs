﻿using Pizzeria.Kinds_of_pizzas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pizzeria
{
    public class SendMessageToMailVisitor : IVisitor
    {
        public void VisitClient(Client client, Pizza pizza)
        {
            Console.WriteLine($"Send message to client with mail { client.Email } that { pizza.Title } added on our pizzeria!");
        }
    }
}

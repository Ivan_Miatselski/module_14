﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pizzeria.Kinds_of_pizzas
{
    public abstract class Pizza
    {
        public string Title { get; }

        public Pizza(string title)
        {
            if (string.IsNullOrWhiteSpace(title))
            {
                throw new ArgumentNullException("Pizza's title should be determined!");
            }

            this.Title = title;
        }

        public abstract int GetPrice();
    }
}

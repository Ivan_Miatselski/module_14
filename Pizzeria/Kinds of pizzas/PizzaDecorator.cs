﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pizzeria.Kinds_of_pizzas
{
    public abstract class PizzaDecorator : Pizza
    {
        protected Pizza pizza;

        public PizzaDecorator(string title, Pizza pizza) : base(title)
        {
            if (pizza == null)
            {
                throw new ArgumentNullException("Object pizza can't be null");
            }

            this.pizza = pizza;
        }
    }
}

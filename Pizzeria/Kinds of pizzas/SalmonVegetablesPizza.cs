﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pizzeria.Kinds_of_pizzas
{
    class SalmonVegetablesPizza : PizzaDecorator
    {
        public SalmonVegetablesPizza(Pizza pizza) : base(pizza.Title + "salmon, vegetables", pizza)
        {}

        public override int GetPrice()
        {
            return pizza.GetPrice() + 1;
        }
    }
}

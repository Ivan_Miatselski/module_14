﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pizzeria.Kinds_of_pizzas
{
    class MushroomsOnionPizza : PizzaDecorator
    {
        public MushroomsOnionPizza(Pizza pizza) : base(pizza.Title + "mushrooms, onion", pizza)
        {}

        public override int GetPrice()
        {
            return pizza.GetPrice() + 2;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pizzeria.Kinds_of_pizzas
{
    public class CustomPizza : Pizza
    {
        public CustomPizza() : base("Best pizza with products: ")
        {}

        public override int GetPrice()
        {
            return 3;
        }
    }
}

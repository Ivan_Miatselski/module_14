﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pizzeria.Kinds_of_pizzas
{
    class MeatPineapplePizza : PizzaDecorator
    {
        public MeatPineapplePizza(Pizza pizza) : base(pizza.Title + "meat, pineapple", pizza)
        {}

        public override int GetPrice()
        {
            return pizza.GetPrice() + 5;
        }
    }
}

﻿using Pizzeria.Kinds_of_pizzas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pizzeria
{
    public class Client : IClient
    {
        public string PassportId { get; protected set; }
        public string Email { get; protected set; }

        public Client(string passportId, string email)
        {
            if (string.IsNullOrWhiteSpace(passportId) || string.IsNullOrWhiteSpace(email))
            {
                throw new ArgumentException("Client cannot without initial properies");
            }

            this.PassportId = passportId;
            this.Email = email;
        }

        public void Accept(IVisitor visitor, Pizza pizza)
        {
            visitor.VisitClient(this, pizza);
        }
    }
}

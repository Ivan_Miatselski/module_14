﻿using Pizzeria.Kinds_of_pizzas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pizzeria
{
    public interface IVisitor
    {
        void VisitClient(Client client, Pizza pizza);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pizzeria.Kinds_of_pizzas;

namespace Pizzeria.PizzaFactory
{
    public abstract class AbstractFactory
    {
        public abstract Pizza CookChickenCheesePizza();
        public abstract Pizza CookMeatPineapple();
        public abstract Pizza CookMushroomsOnionPizza();
        public abstract Pizza CookSalmonVegetablesPizza();
    }
}

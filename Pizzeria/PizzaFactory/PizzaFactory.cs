﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pizzeria.Kinds_of_pizzas;

namespace Pizzeria.PizzaFactory
{
    public class PizzaFactory : AbstractFactory
    {
        private Pizza pizza = new CustomPizza();

        public override Pizza CookChickenCheesePizza()
        {
            return new ChickenCheesePizza(this.pizza);
        }

        public override Pizza CookMeatPineapple()
        {
            return new MeatPineapplePizza(this.pizza);
        }

        public override Pizza CookMushroomsOnionPizza()
        {
            return new MushroomsOnionPizza(this.pizza);
        }

        public override Pizza CookSalmonVegetablesPizza()
        {
            return new SalmonVegetablesPizza(this.pizza);
        }
    }
}

﻿using Pizzeria.Kinds_of_pizzas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmulationWorkPizzeria
{
    class Program
    {
        static void Main(string[] args)
        {
            var factory = new Pizzeria.PizzaFactory.PizzaFactory();
            var pizzeria = Pizzeria.Pizzeria.GetInstance(factory);

            Pizzeria.IClient client1 = new Pizzeria.Client("MP3245299", "1@mail.ru");
            Pizzeria.IClient client2 = new Pizzeria.Client("MP0399324", "2@mail.ru");

            pizzeria.SubscribeToSendingNotification(client1);
            pizzeria.SubscribeToSendingNotification(client2);

            Pizza pizza = new CustomPizza();
            СaviarPizza caviarPizza = new СaviarPizza(pizza);

            pizzeria.Accept(new Pizzeria.SendMessageToMailVisitor(), caviarPizza);

            Console.ReadKey();
        }
    }
}

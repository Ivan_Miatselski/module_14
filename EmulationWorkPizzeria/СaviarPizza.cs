﻿using Pizzeria.Kinds_of_pizzas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmulationWorkPizzeria
{
    public class СaviarPizza : PizzaDecorator
    {
        public СaviarPizza(Pizza pizza) : base(pizza.Title + "caviar", pizza)
        {}

        public override int GetPrice()
        {
            return pizza.GetPrice() + 10;
        }
    }
}
